export interface IPortfolio {
  token: string;
  value: number;
  balance: number;
  currentPrice: number;
  latestTransactionType: string;
  latestTransactionAmount: number;
}