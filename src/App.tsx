import "./App.css";
import * as React from "react";
import logo from "./logo.svg";
import { IPortfolio } from "./model/IPortfolio";
import { IntlProvider, FormattedNumber } from "react-intl";
import styled from "styled-components/macro";

const Row = styled.div`
  text-align: center;
  align-items: flex-start;
  flex-direction: row;
  font-size: 24px;
  font-weight: bold;
`;

const Container = styled.div`
  align-content: center;
  justify-content: center;
`;

const Space = styled.div`
  height: 20px;
`;

export type State = {
  data: IPortfolio[];
  totalBalance: number;
};

class App extends React.Component<{}, State> {
  constructor(props: any) {
    super(props);
    this.state = {
      data: [],
      totalBalance: 0,
    };
  }

  public componentDidMount() {
    const ws = new WebSocket("ws://128.199.239.139:8999");

    ws.onmessage = (evt: MessageEvent) => {
      const data = JSON.parse(evt.data);

      const portfolios: IPortfolio[] = data.portfolios;
      const totalBalance = this.getTotalBalance(data.portfolios);
      console.log("Data", data);
      this.setState({
        data: portfolios,
        totalBalance: totalBalance,
      });
    };
  }

  public getTotalBalance(portfolios: IPortfolio[]): number {
    let totalBalance = 0;
    for (let i = 0; i < portfolios.length; i++) {
      totalBalance += portfolios[i].balance;
    }
    return totalBalance;
  }

  public renderExchangeTab() {
    return this.state.data.map((item: IPortfolio) => {
      return (
        <div key={item.token}>
          <Row>
            {item.token} {"  "}
            <FormattedNumber
              value={item.currentPrice}
              style="currency"
              currency={"USD"}
              minimumFractionDigits={2}
              maximumFractionDigits={5}
            />
          </Row>
          <Space />
        </div>
      );
    });
  }

  public renderSpot() {
    return this.state.data.map((item: IPortfolio) => {
      return (
        <div key={item.token}>
          {/* <h3>
            {item.token}: {item.balance} USD
          </h3> */}
          <Row>
            {item.token} {"  "}
            {item.value}
            {" ~~~ "}
            <FormattedNumber
              value={item.balance}
              style="currency"
              currency={"USD"}
              minimumFractionDigits={2}
              maximumFractionDigits={5}
            />
          </Row>
          <Space />
        </div>
      );
    });
  }

  public render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h3 className="App-title">My Wallet</h3>
        </header>
        <Container>
          <IntlProvider locale="en">
            <h2>My Spot</h2>
            <Row>
              Total Balance: {"   "}
              <FormattedNumber
                value={this.state.totalBalance}
                style="currency"
                currency={"USD"}
                minimumFractionDigits={2}
                maximumFractionDigits={5}
              />
            </Row>
            <Space />
            {this.renderSpot()}
            <h2>Exchange</h2>
            {this.renderExchangeTab()}
          </IntlProvider>
        </Container>
      </div>
    );
  }
}

export default App;
